<?php

namespace App\Helpers;

/**
 * Хелпер для консистентости кэш-ключей
 */
class CacheKeys
{
    public static function patientById($id)
    {
        return 'patient_' . $id;
    }

    public static function patientList()
    {
        return 'patients';
    }
}
