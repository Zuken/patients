<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Patient extends Model
{
    use HasFactory;

    public const AGE_TYPE_DAYS = 'день';
    public const AGE_TYPE_MONTHS = 'месяц';
    public const AGE_TYPE_YEARS = 'год';
    public $timestamps = false;

    protected $casts = [
        'birthdate' => 'date',
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'birthdate'
    ];

    protected $guarded = [
        'age',
        'age_type'
    ];

    public function setBirthdateAttribute($value)
    {
        $this->attributes['birthdate'] = Carbon::parse($value)->toDateString();
        $this->recalculateAge();

        return $this;
    }

    /**
     * Пересчитывает возраст пацианта в зависимости от даты рождения.
     * @return void
     */
    protected function recalculateAge(): void
    {
        $birthdate = Carbon::make($this->birthdate);

        $age = $birthdate->diffInYears();
        $age_type = static::AGE_TYPE_YEARS;

        if ($age <= 0) {
            $age = $birthdate->diffInMonths();
            $age_type = static::AGE_TYPE_MONTHS;
            if ($age <= 0) {
                $age = $birthdate->diffInDays();
                $age_type = static::AGE_TYPE_DAYS;
            }
        }

        $this->age = $age;
        $this->age_type = $age_type;
    }
}
