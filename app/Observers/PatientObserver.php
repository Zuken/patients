<?php

namespace App\Observers;

use Illuminate\Support\Facades\Cache;
use App\Helpers\CacheKeys;

class PatientObserver
{
    public function created()
    {
        Cache::forget(CacheKeys::patientList());
    }

    public function updated($model)
    {
        Cache::forget(CacheKeys::patientList());
        Cache::forget(CacheKeys::patientById($model->id));
    }

    public function deleted($model)
    {
        Cache::forget(CacheKeys::patientList());
        Cache::forget(CacheKeys::patientById($model->id));
    }

    public function restored($model)
    {
        Cache::forget(CacheKeys::patientList());
        Cache::forget(CacheKeys::patientById($model->id));
    }
}
