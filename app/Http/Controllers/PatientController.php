<?php

namespace App\Http\Controllers;

use App\Helpers\CacheKeys;
use App\Http\Resources\PatientCollection;
use App\Jobs\ProcessPatient;
use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class PatientController extends Controller
{
    public function create(Request $request)
    {
        $patient = Patient::create([
            'first_name' => $request->string('first_name'),
            'last_name' => $request->string('last_name'),
            'birthdate' => $request->string('birthdate')
        ]);

        Cache::remember(CacheKeys::patientById($patient->id), 300, fn () => $patient);

        ProcessPatient::dispatch($patient);

        return response(status: 201);
    }

    public function list()
    {
        $patients = Cache::remember(CacheKeys::patientList(), 300, fn () => Patient::orderByDesc('id')->get()); // тут должна быть пагинация

        return new PatientCollection($patients);
    }
}
