<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PatientController;

Route::controller(PatientController::class)->group(function () {
    Route::get('/patients', 'list');
    Route::post('/patient', 'create');
});
