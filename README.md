## Routes
- POST `/api/patient` - создание сущности "Пациент"
- GET `/api/patients` - получение списка пациентов, отсортированных по убыванию `id`

## Classes

`App\Http\Controllers\PatientController` - контроллер Пациента; 

`App\Models\Patient` - модель "Пациент" 

`App\Http\Resources\PatientResource` - Resource для модели `Patient`

`App\Jobs\ProcessPatient` - джоба Пациента

`App\Observers\PatientObserver` - наблюдатель модели `Patient` для чистки кэша.

`App\Helpers\CacheKeys` - Хелпер для консистентости кэш-ключей

P.S: Возможно, я понял задание слишком буквально, но на самом деле, я бы не хранил вычисляемые поля `age` и `age_type` в базе, т.к. они были бы неконсистентные, если запись не обновляется, а в случае с `age_type - день`, его нужно обновлять запись каждый день. Поэтому, я бы высчитавал эти поля в `PatientResource`
